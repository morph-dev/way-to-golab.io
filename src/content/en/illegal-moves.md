You can play almost anywhere on the board, but there are some places where the
rules say you cannot.

White is allowed to play at the triangle.

If black were to play there, that stone would already be surrounded so black is
not allowed to play there.

Board::illegal

---

In this case, however, black is allowed to play at triangle because doing so
would capture two white stones.

Board::legal

---

After black has placed a stone there, this would be the board position.

Board::captured
