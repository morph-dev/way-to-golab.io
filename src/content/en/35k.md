## Approaching end game

As the game approaches the end and both black and white's territories are
settled, all you have to do is to decide the border lines clearly.

At this stage, you must try to push the border toward the opponent's side,
widening your territories as large as possible while reducing the opponent's.

---

Black occupies left side and white controls the right.  However, there are
still a few places where the border is not clear.

Where is the point that you can get the most profit?

Board::yose-1

---

Black has the left and white has the right. The game is almost over, but be
careful until the very end!

White just played the marked move.  If you ignore it, your precious territory
will be ruined.

Board::yose-2
