In questa lezione impareremo come funzionano le scale.

Cattura la pietra bianca. Continua a dare atari consecutivi mentre bianco
cerca di scappare.

Board::ladder-1

Hai capito?

Chiamiamo questa forma **scala** perchè la figura risultante finisce per assomigliare
ad una serie di scalini.

---

Ecco un'altra scala.

Sfortunatamente, è il tuo turno di scappare. Tuttavia, questa volta, scappare è
possibile.

Buona fortuna!

Board::ladder-2

Puoi scappare dagli attacchi persistenti di bianco?

Se ci sono pietre amiche sul percorso della scala, puoi scappare dall'attacco perchè
puoi aumentare le tue libertà collegandoti.

## Proverbi di Go

I [proverbi di Go][go-proverbs] vengono creati dalla community per riassumere saggezza in
piccole frase facili da ricordare.

Sentirai spesso queste pillole di saggezza dispensate durante i commenti di una partità 
o durante delle lezioni.

Ecco il proverbio per questa lezione.

> Non giocare a Go se non conosci la scala.

L'idea principale dietro questo proverbio è che imparare la scala non è molto difficile
rispetto ad imparare il gioco del Go nella sua interezza. Se uno decide di giocare a Go,
dovrebbe investire del tempo per imparare la scala.

[go-proverbs]: http://senseis.xmp.net/?GoProverbs
