### Estendere la tua pietra

Questa volta, sei tu ad attaccare.

Hai giocato a contatto della pietra bianca in E7 dopo di che bianco ha
contrattaccato con F6.

Qual è la mossa migliore ora?

Board::extend0

---

Quando estendi dalla tua pietra in E6 giocando in D6, stai rinforzando le pietre
aumentandone le libertà da 2 a 4.

Board::extend1

---

Puoi anche estendere verso l'alto come mostrato di seguito. Verso quale direzione
dovresti estendere dipende dalla situazione. Non devi pensarci per ora, limitati a
ricordare di "estendere". 

Board::extend2

---

Se nero non avesse esteso, cosa sarebbe accaduto?

Probabilmente, bianco attaccherebbe la pietra in E6. Avresti dovuto continuare ad
attaccare bianco dopo aver giocato in E6, toccando E7.

Board::extend3

---

Ora è tempo di far pratica. Estendi la tua pietra dopo aver giocato a contato ed
essere stato schiaffeggiato. Ci sono due risposte corrette. Trovale entrambe!

Board::extend4

Ricorda che una mossa di estensione può proteggere le tue pietre.
