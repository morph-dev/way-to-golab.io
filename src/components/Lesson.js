import AutoResponse from './AutoResponse';
import Freeplay from './Freeplay';
import Replay from './Replay';
import StaticBoard from './StaticBoard';
import CommonMark from 'commonmark';
import React from 'react';
import ReactRenderer from 'commonmark-react-renderer';
import content from '../content';
import languages from '../languages';
import {startsWith} from 'lodash';
import {useAsync} from 'react-async';

export default function Lesson({locale, page}) {
    const {data: pageContent, isPending: loadingContent} = useAsync({
        promiseFn: content,
        watch: `./${locale}/${page}.md`,
        path: `./${locale}/${page}.md`,
    });
    const {data: missingContent, isPending: loading404} = useAsync({
        promiseFn: content,
        watch: `./${locale}/404.md`,
        path: `./${locale}/404.md`,
    });
    const {data: pagePositions, isPending: loadingPositions} = useAsync({
        promiseFn: content,
        watch: `./positions/${page}.yaml`,
        path: `./positions/${page}.yaml`,
    });

    if (loadingContent || loading404 || loadingPositions) {
        return null;
    }

    const contentToRender = pageContent || missingContent;
    const parser = new CommonMark.Parser();
    const renderer = new ReactRenderer();

    return <div className='lesson'>
        {renderer.render(parser.parse(contentToRender)).map(
            (parsed, index) => {
                const inner = parsed.props.children && parsed.props.children[0];
                if (startsWith(inner, 'Board::')) {
                    const boardKey = inner.split('::')[1];
                    const boardProps = pagePositions[boardKey];
                    const listKey = `${locale}:${page}:${boardKey}`;

                    switch(boardProps.type) {
                        case 'auto-response':
                            return <AutoResponse key={listKey} {...boardProps}/>;
                        case 'freeplay':
                            return <Freeplay key={listKey} {...boardProps}/>;
                        case 'replay':
                            return <Replay key={listKey} {...boardProps}/>;
                        case 'static':
                            return <StaticBoard key={listKey} {...boardProps}/>;
                        default:
                            return null;
                    }
                } else if (inner === '::Translators::') {
                    return <ul key={'translators-' + index}>{languages.map(
                        (language, index) => <li key={'translator-' + index}>
                            {language.name}: {language.translators.map(
                                (translator, index) => <span key={'span-' + index}>
                                    {translator.link ?
                                        <a href={translator.link} target='_blank' rel='noreferrer'>
                                            {translator.name}
                                        </a>
                                        : translator.name
                                    }
                                    {index === language.translators.length - 1 ? '' : ', '}
                                </span>
                            )}
                        </li>
                    )}</ul>;
                }
                return parsed;
            }
        )}
    </div>;
}
